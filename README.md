# README #

Aplicativo demonstrativo para a listagem de laptops da Best Buy através da API Rest oficial

Principais recursos do aplicativo:

* Listagem de produtos ordenados por preço
* Visualização dos detalhes dos laptops
* Pesquisa dos laptops
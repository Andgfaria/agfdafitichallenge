/* AGFFocoImagemViewController - classe criada por André Gimenez Faria em janeiro de 2016
   UIViewController para exibição de imagens, tendo a possibilidade de usar um zoom
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit

class AGFFocoImagemViewController: UIViewController, UIScrollViewDelegate {

    var imagem : UIImage!
    
    @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configuração dos limites de escala para o zoom da imagem
        scrollView.minimumZoomScale = 1.0
        scrollView.maximumZoomScale = 5.0
        imageView.image = imagem
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(animated: Bool) {
        // Ocultamento inicial da UINavigationBar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    @IBAction func sair(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return imageView
    }

    // Nessa tela com um toque é possível ocultar e mostrar a UINavigationBar
    @IBAction func tratarToqueNaScrollView(sender: AnyObject) {
        self.navigationController?.setNavigationBarHidden(!self.navigationController!.navigationBarHidden, animated: true)
    }

}

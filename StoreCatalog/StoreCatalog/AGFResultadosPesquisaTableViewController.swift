/* AGFResultadosPesquisaTableViewController - classe criada por André Gimenez Faria em janeiro de 2016
   UITableViewController para exibição dos resultados pesquisa
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import SDWebImage

class AGFResultadosPesquisaTableViewController: UITableViewController {

    private var laptopSelecionado = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("recarregarTabela"), name: "pesquisaConcluida", object: nil)
        // Alteração do padding da tabela. iPads possuem padding maior e iPhones possuem padding menor
        self.tableView.contentInset =  self.view.traitCollection.horizontalSizeClass == .Regular && self.view.traitCollection.verticalSizeClass == .Regular ? UIEdgeInsetsMake(108, 0, 44, 0)  : UIEdgeInsetsMake(64, 0, 0, 0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().cleanDisk()
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AGFCachePesquisa.sharedInstance.laptops?.count ?? 0
    }

    /* Componentes da célula
        .1 - UIImageView para exibir a foto do laptop
        .2 - UILabel para exibir o nome do laptop
        .3 - UILabel apra exibir a fabricante do laptop
    
    */
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("celulaLaptop", forIndexPath: indexPath)
        let laptop = AGFCachePesquisa.sharedInstance.laptops![indexPath.row]
        let imageViewLaptop = cell.viewWithTag(1) as! UIImageView
        if laptop.urlImagem != nil {
            imageViewLaptop.sd_setImageWithURL(NSURL(string: laptop.urlImagem!), placeholderImage: nil, completed: { (image, error, cachetype, url) -> Void in
                if error != nil {
                    imageViewLaptop.image = UIImage(named: "imgLaptop")
                }
            })
        }
        let labelNomeLaptop = cell.viewWithTag(2) as! UILabel
        labelNomeLaptop.text = laptop.nome
        let labelFabricante = cell.viewWithTag(3) as! UILabel
        labelFabricante.text = laptop.fabricante
        cell.backgroundColor = UIColor.clearColor()
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        laptopSelecionado = indexPath.row
        // Exibição da tela de detalhes do produto
        self.performSegueWithIdentifier("detalhesLaptop", sender: self)
    }
    
    func recarregarTabela() {
        self.tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "detalhesLaptop" {
            (segue.destinationViewController as! AGFDetalhesLaptopViewController).laptop = AGFCachePesquisa.sharedInstance.laptops![laptopSelecionado]
            laptopSelecionado = -1
        }
    }

}

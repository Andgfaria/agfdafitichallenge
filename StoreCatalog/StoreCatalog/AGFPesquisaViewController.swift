/* AGFPesquisaViewController - classe criada por André Gimenez Faria em janeiro de 2016
   UIViewController para exibição dos componentes de pesquisa do aplicativo
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import SDWebImage

class AGFPesquisaViewController: UIViewController, UISearchBarDelegate, UITextFieldDelegate {
    
    private var barraPesquisa = UISearchBar(frame: CGRectZero)

    // UIView onde a barra de pesquisa será exibida
    private var containerBarraPesquisa = UIView(frame: CGRectMake(0,0,320,44))
    
    // UIView contendo a tabela de resultados
    @IBOutlet var containerTabelaResultados: UIView!
    
    // UILabel para exibir mensagens de erro ou ausência de produtos
    @IBOutlet var labelMensagem: UILabel!
    
    @IBOutlet var indicadorAtividade: UIActivityIndicatorView!
    
    // Referência da contrainst do atributo bottom da view que contém os elementos de pesquisa
    @IBOutlet var bottomConstraintContainerPesquisa: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Configuração da barra de pesquisa
        barraPesquisa.text = AGFCachePesquisa.sharedInstance.textoPesquisa
        barraPesquisa.placeholder = NSLocalizedString("pesquisarLaptops", comment: "")
        barraPesquisa.delegate = self
        barraPesquisa.searchBarStyle = UISearchBarStyle.Minimal
        // Posicionamento da barra de pesquisa dentro da view de container e configuração das constraints
        containerBarraPesquisa.addSubview(barraPesquisa)
        barraPesquisa.translatesAutoresizingMaskIntoConstraints = false
        containerBarraPesquisa.addConstraint(NSLayoutConstraint(item: barraPesquisa, attribute: NSLayoutAttribute.Width, relatedBy: NSLayoutRelation.Equal, toItem: containerBarraPesquisa, attribute: NSLayoutAttribute.Width, multiplier: 1, constant: 0))
         containerBarraPesquisa.addConstraint(NSLayoutConstraint(item: barraPesquisa, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: containerBarraPesquisa, attribute: NSLayoutAttribute.Height, multiplier: 1, constant: 0))
         containerBarraPesquisa.addConstraint(NSLayoutConstraint(item: barraPesquisa, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: containerBarraPesquisa, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0))
         containerBarraPesquisa.addConstraint(NSLayoutConstraint(item: barraPesquisa, attribute: NSLayoutAttribute.CenterY, relatedBy: NSLayoutRelation.Equal, toItem: containerBarraPesquisa, attribute: NSLayoutAttribute.CenterY, multiplier: 1, constant: 0))
        containerBarraPesquisa.layoutIfNeeded()
        // Configuração da textfield da barra de pesquisa
        let textFieldPesquisa = barraPesquisa.valueForKey("_searchField") as! UITextField
        textFieldPesquisa.tintColor = self.navigationController?.navigationBar.tintColor
        textFieldPesquisa.textColor = UIColor.whiteColor()
        // Notificação para tratar a exibição do teclado
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("adaptarLayoutParaTecladoExibido:"), name: UIKeyboardWillShowNotification, object: nil)
        // Notificação para tratar quando o teclado é dispensado
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("adaptarLayoutParaTecladoOcultado"), name: UIKeyboardWillHideNotification, object: nil)
        // Notificação para tratar a conclusão da requisição de pesquisa
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("tratarResultadosPesquisa"), name: "pesquisaConcluida", object: nil)
    }
    
    override func viewWillAppear(animated: Bool) {
        // Inserção do container da barra de pesquisa na UINavigationBar
        self.navigationItem.titleView = containerBarraPesquisa
        /* Se o texto da barra de pesquisa estiver vazio o teclado é invocado.
           Caso contrário, a interface é adaptada de acordo com a busca feita anteriormente.
        */
        if !barraPesquisa.text!.isEmpty {
            if AGFCachePesquisa.sharedInstance.laptops?.count > 0 {
                containerTabelaResultados.hidden = false
                indicadorAtividade.hidden = true
                labelMensagem.hidden = true
            }
            else {
                containerTabelaResultados.hidden = true
                indicadorAtividade.hidden = true
                labelMensagem.hidden = false
            }
        }
        else {
            barraPesquisa.becomeFirstResponder()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().cleanDisk()
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        if searchBar.text!.isEmpty {
            self.dismissViewControllerAnimated(true, completion: nil)
        }
    }
    
    func searchBar(searchBar: UISearchBar, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        // Bloqueio de caracteres de símbolos ou pontuação para a barra de pesquisa
        return (text.rangeOfCharacterFromSet(NSCharacterSet.punctuationCharacterSet()) == nil) && (text.rangeOfCharacterFromSet(NSCharacterSet.symbolCharacterSet()) == nil)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        if !searchText.isEmpty {
            containerTabelaResultados.hidden = true
            labelMensagem.hidden = true
            indicadorAtividade.hidden = false
            // Requisição de pesquisa com a entrada do usuário
            AGFCachePesquisa.sharedInstance.textoPesquisa = searchText
        }
        else {
            containerTabelaResultados.hidden = true
            labelMensagem.hidden = true
            indicadorAtividade.hidden = true
            AGFCachePesquisa.sharedInstance.limpar()
        }
    }
    
    func tratarResultadosPesquisa() {
        if !barraPesquisa.text!.isEmpty {
            // Exibição dos resultados da pesquisa
            if AGFCachePesquisa.sharedInstance.laptops?.count > 0 {
                indicadorAtividade.hidden = true
                labelMensagem.hidden = true
                containerTabelaResultados.hidden = false
            }
            // Indicação da ausência de produtos ou erro de rede
            else {
                indicadorAtividade.hidden = true
                labelMensagem.hidden = false
                labelMensagem.text = AGFCachePesquisa.sharedInstance.laptops == nil ? NSLocalizedString("erroRede", comment: "") : NSLocalizedString("semLaptops", comment: "")
                containerTabelaResultados.hidden = true
            }
        }
    }
    
    // Alteração do layout após a exibição do teclado
    func adaptarLayoutParaTecladoExibido(notificacao : NSNotification) {
        let frameTeclado = notificacao.userInfo![UIKeyboardFrameEndUserInfoKey]!.CGRectValue
        let alturaTeclado = frameTeclado.height
        UIView.animateWithDuration(0.2, animations: {
            self.bottomConstraintContainerPesquisa.constant = -alturaTeclado
            self.view.layoutIfNeeded()
        })
    }
    
    // Alteração do layout após o teclado ser dispensado
    func adaptarLayoutParaTecladoOcultado() {
        UIView.animateWithDuration(0.2, animations: {
            self.bottomConstraintContainerPesquisa.constant = 0
            self.view.layoutIfNeeded()
        })
    }
    
    @IBAction func sair(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

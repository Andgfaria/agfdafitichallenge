//
//  AGFAjustesTableViewController.swift
//  StoreCatalog
//
//  Created by André Gimenez Faria on 16/01/16.
//  Copyright © 2016 André Gimenez Faria. All rights reserved.
//

import UIKit
import MessageUI

class AGFAjustesTableViewController: UITableViewController, MFMailComposeViewControllerDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Ocultamento do botão "Done" da tela para os iPads, já que neles a tela é apresentada como PopOver
        if UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiom.Pad {
            self.navigationItem.rightBarButtonItem = nil
        }
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.setNeedsStatusBarAppearanceUpdate()
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.section == 0 {
            cell.tintColor = self.navigationController?.navigationBar.tintColor
            cell.accessoryType = indexPath.row == AGFGerenciadorPreferencias.sharedInstance.preferenciaOrdenacao.rawValue ? UITableViewCellAccessoryType.Checkmark : UITableViewCellAccessoryType.None
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Alteração da preferência de ordenaç˜åo
        if indexPath.section == 0 {
            let novaPreferencia = AGFGerenciadorPreferencias.CriterioOrdenacao(rawValue: indexPath.row)!
            AGFGerenciadorPreferencias.sharedInstance.alterarPreferenciaOrdenacao(novaPreferencia)
            self.tableView.reloadData()
        }
        else if indexPath.section == 1 && indexPath.row == 1 {
            exibirTelaEmail()
        }
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func sair(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // Tela de composição de email
    private func exibirTelaEmail () {
        if MFMailComposeViewController.canSendMail() {
            let telaEmail = MFMailComposeViewController()
            telaEmail.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName : UIColor.blackColor()]
            telaEmail.mailComposeDelegate = self
            telaEmail.setSubject("Aplicativo Laptop Store")
            telaEmail.setToRecipients(["andfaria13@gmail.com"])
            self.presentViewController(telaEmail, animated: true, completion: nil)
        }
    }
    
    func mailComposeController(controller: MFMailComposeViewController, didFinishWithResult result: MFMailComposeResult, error: NSError?) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

}

/* AGFLaptop - Classe criada por André Gimenez Faria em janeiro de 2016
   Essa classe serve como modelo de produto para o aplicativo
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import ObjectMapper

class AGFLaptop: Mappable {

    // SKU do produto
    var id : Int!

    var nome : String!
    
    var fabricante : String?
    
    var descricao : String?
    
    var preco : Double!
    
    //URL para a página do produto
    var url : String!
    
    //URL para a compra do produto
    var urlCarrinho : String!
    
    var urlImagem : String?
    
    var urlImagemGrande : String?
    
    var disponivelOnline : Bool!
    
    required init?(_ map: Map){
        
    }
    
    // Função que faz o mapeamento JSON x Objeto para essa classe utilizando o ObjectMapper
    func mapping(map: Map) {
        id <- map["sku"]
        fabricante <- map["manufacturer"]
        nome <- map["name"]
        /* O nome dos laptops inclui também o nome da fabricante.
           Porém essa informação torna-se redudante, já que o nome da fabricante é exibido separadamente.
           Sendo assim, é que aqui retirado do início da string o nome da fabricante do laptop.
        */
        let numeroCaracteresFabricante = fabricante?.characters.count ?? 0
        let rangeStringSemNomeFabricante = nome.startIndex.advancedBy(numeroCaracteresFabricante + 3)..<nome.endIndex
        nome = nome.substringWithRange(rangeStringSemNomeFabricante)
        descricao <- map["longDescription"]
        preco <- map["salePrice"]
        url <- map["url"]
        urlCarrinho <- map["addToCartUrl"]
        urlImagem <- map["image"]
        urlImagemGrande <- map["largeFrontImage"]
        disponivelOnline <- map["onlineAvailability"]
    }
    
    
}

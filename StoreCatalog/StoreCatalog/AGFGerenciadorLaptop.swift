/* AGFGerenciadorLaptop - classe criada por André Gimenez Faria em janeiro de 2016
   Classe responsável pela interação do aplicativo com a API da Best Buy
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import Alamofire
import ObjectMapper

private var requestPesquisa : Request?

class AGFGerenciadorLaptop: NSObject {
    
    // Singleton da classe
    class var sharedInstance : AGFGerenciadorLaptop {
        struct Static {
            static var instance : AGFGerenciadorLaptop?
            static var token    : dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = AGFGerenciadorLaptop()
        }
        return Static.instance!
    }
    
    // URL base da API da BestBuy
    static let BASE_URL = "https://api.bestbuy.com/v1/products"

    // Chave para uso da API da BestBuy
    static let API_KEY = "f5v8x7pkaqevnupj6z5t2gbm"

    // Identificador da categoria dos laptops
    static let CategoriaLaptops = "(categoryPath.id=abcat0502000)"
    
    // Método que retorna o array de produtos no JSON e retorna um array de objetos mapeados
    private static func mapearListaLaptopsDeJsonParaObjeto(Laptops : [AnyObject]) -> [AGFLaptop]? {
        var listaLaptops : [AGFLaptop] = []
        for p in Laptops {
            let laptop = Mapper<AGFLaptop>().map(p)
            listaLaptops.append(laptop!)
        }
        return listaLaptops
    }
    
    /* Método que faz uma requisição dos laptops mais baratos ou mais caros
       O primeiro parâmetro indica a página a ser retornada (por padrão, a primeira)
       O segundo paramêtro é um callback a ser executado após o fim da requisição para tratar erros e atualizar a interface
    */
    static func laptopsEmDestaque(pagina : Int = 1, callback : [AGFLaptop]?->()) {
        Alamofire.request(.GET, BASE_URL + "(" + CategoriaLaptops + ")", parameters: ["apiKey" : API_KEY, "sort" : AGFGerenciadorPreferencias.sharedInstance.preferenciaOrdenacao == AGFGerenciadorPreferencias.CriterioOrdenacao.MaisBaratos ? "salePrice.asc" : "salePrice.desc", "page" : pagina,"pageSize" : 25, "format" : "json"]).responseJSON { response in
            if response.result.isSuccess {
                // Produtos obtidos
                if let Laptops = response.result.value?["products"] as? [AnyObject] {
                    let listaLaptops = mapearListaLaptopsDeJsonParaObjeto(Laptops)
                    callback(listaLaptops)
                }
                // Problema no JSON
                else {
                    callback(nil)
                }
            }
            // Erro de rede
            else {
                callback(nil)
            }
        }
    }
    
    /* Método que faz a pesquisa de laptops por palavras-chave
        O primeiro parâmetro é um vetor em que cada elemento é uma palavra-chave a ser buscada
        O segundo paramêtro é um callback a ser executado após o fim da requisição para tratar erros e atualizar a interface
    */
    func pesquisar(termosPesquisa : [String], callback : ([AGFLaptop]?->())) {
        // Cancelamento da pesquisa anterior, caso tenha sido feita
        requestPesquisa?.cancel()
        var parametrosPesquisa = ""
        // Montagem da query de pesquisa
        for (var i = 0; i < termosPesquisa.count; i++) {
            if !termosPesquisa[i].isEmpty {
                if i > 0 {
                    parametrosPesquisa += "?"
                }
                parametrosPesquisa += "search=" + termosPesquisa[i].stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet())!
            }
        }
        requestPesquisa =  Alamofire.request(.GET, AGFGerenciadorLaptop.BASE_URL + "((" +  parametrosPesquisa + ")&" + AGFGerenciadorLaptop.CategoriaLaptops + ")", parameters: ["apiKey" : AGFGerenciadorLaptop.API_KEY, "pageSize" : 25, "format" : "json"]).responseJSON { response in
            // Garantia que se uma pesquisa foi concluída depois que outra pesquisa foi iniciada não haverá concorrência
            if response.request != requestPesquisa?.request {
                return
            }
            if response.result.isSuccess {
                // Produtos encontrados
                if let Laptops = response.result.value?["products"] as? [AnyObject] {
                    let listaLaptops = AGFGerenciadorLaptop.mapearListaLaptopsDeJsonParaObjeto(Laptops)
                    callback(listaLaptops)
                }
                // Problem ano JSON
                else {
                    callback(nil)
                }
            }
            // Erro de rede
            else {
                callback(nil)
            }
        }

    }
    
    func cancelarPesquisa() {
        requestPesquisa?.cancel()
    }
    
}

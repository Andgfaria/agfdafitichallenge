/* AGFGerenciadorPreferencias - classe criada por André Gimenez Faria em janeiro de 2016
   Classe para gerenciar as preferências do usuário no aplicativo
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit

class AGFGerenciadorPreferencias: NSObject {

    // Definição das opções de ordenação disponíveis
    enum CriterioOrdenacao : Int {
        case MaisBaratos = 0, maisCaros
    }
    
    var preferenciaOrdenacao : CriterioOrdenacao = CriterioOrdenacao(rawValue: NSUserDefaults.standardUserDefaults().integerForKey("preferenciaOrdenacao")) ?? CriterioOrdenacao.MaisBaratos
    
    // Singleton da classe
    class var sharedInstance : AGFGerenciadorPreferencias {
        struct Static {
            static var instance : AGFGerenciadorPreferencias?
            static var token    : dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = AGFGerenciadorPreferencias()
        }
        return Static.instance!
    }
    
    func alterarPreferenciaOrdenacao(novaPreferencia : CriterioOrdenacao) {
        preferenciaOrdenacao = novaPreferencia
        NSUserDefaults.standardUserDefaults().setInteger(preferenciaOrdenacao.rawValue, forKey: "preferenciaOrdenacao")
        NSUserDefaults.standardUserDefaults().synchronize()
        NSNotificationCenter.defaultCenter().postNotificationName("preferenciaOrdenacaoAlterada", object: self)
    }
    
}

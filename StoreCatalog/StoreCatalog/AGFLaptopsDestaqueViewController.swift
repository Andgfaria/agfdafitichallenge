/* AGFLaptopsDestaqueViewController - Classe criada por André Gimenez Faria em janeiro de 2016
   UIViewController principal do aplicativo, onde são exibidos em um grid laptops da BestBuy
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import SDWebImage

class AGFLaptopsDestaqueViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    //Array dos objetos que representam os laptops exibidos nessa tela
    private var laptops : [AGFLaptop]?

    //Flag para garantir que, em uma única vez, seja feita a requisição para carregar o conteúdo quando a tela for exibida.
    private var primeiraVisualizacao = true
    
    @IBOutlet var collectionViewLaptops: UICollectionView!
    
    //Label para indicar erros de rede ou a ausência de laptops.
    @IBOutlet var labelInformacao: UILabel!
    
    //Botão para refazer a requisição de conteúdo no caso de falha
    @IBOutlet var botaoTentarNovamente: UIButton!
    
    @IBOutlet var indicadorAtividade: UIActivityIndicatorView!
    
    private var refreshControl = UIRefreshControl()
    
    //Variável para salvar o índice do item selecionado da Collection View (utilizada no método prepareForSegue
    private var laptopSelecionado = -1
    
    /* Referência da última célula da colletiom view, em que o usuário pode requisitar mais produtos para serem exibidos.
       É guardada essa referência para que os componentes de interface da célula possam ser manipulados em tempo de execução.
    */
    private var celulaCarregarMaisLaptops : UICollectionViewCell!
    
    /* Variável que contém o índice da última página carregada.
       Cada requisição retorna até 25 produtos por vez.
    */
    private var paginaAtual = 1
    
    // Flag para garantir que não haja concorrência entre requisições para carregar as próximas páginas de produtos
    private var carregandoMaisLaptops = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Tratamento de notificações de rotação do dispositivo para ajustar o tamanho das células da Collection View
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("orientacaoTrocada:"), name: UIDeviceOrientationDidChangeNotification, object: nil)
        //Mudança do padding da Collection View de acordo com a resolução da tela.
        if self.view.traitCollection.horizontalSizeClass == .Regular && self.view.traitCollection.verticalSizeClass == .Regular{
            collectionViewLaptops.contentInset = UIEdgeInsetsMake(84, 0, 24, 0)
        }
        else {
            collectionViewLaptops.contentInset = UIEdgeInsetsMake(64, 0, 0, 0)
        }
        // Configuração do UIRefreshControl vinculado à collection view da tela.
        refreshControl.tintColor = self.navigationController?.navigationBar.barTintColor
        refreshControl.addTarget(self, action: Selector("recarregarLaptops"), forControlEvents: UIControlEvents.ValueChanged)
        collectionViewLaptops.addSubview(refreshControl)
        // Listener da notificação de alteração da preferência de ordenação na tela de ajustes
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("tratarAlteracaoPreferenciaOrdenacao"), name: "preferenciaOrdenacaoAlterada", object: nil)
    }
    
    override func viewDidAppear(animated: Bool) {
        if primeiraVisualizacao {
            primeiraVisualizacao = false
            carregarLaptopsPelaPrimeiraVez()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Esvaziamento do cache do SDImage
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().cleanDisk()
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if laptops != nil {
            // É retornada uma célula a mais para que o usuário possa requisitar mais produtos para serem exibidos
            return laptops!.count + 1
        }
        return 0
    }
    
    /* 2 tipos de células são utilizados aqui
       CelulaLaptop - exibe as informações básicas dos laptops
            .1 - UIImageView para exibir a foto pequena do laptop
            .2 - UILabel para exibir o nome do laptop
            .3 - UILabel para exibir o nome da fabricante do laptop
            .4 - UILabel para exibir o preço do laptop
        CelulaCarregarMaisLaptop = célula em que o usuário pode requisitar mais produtos
            .1 - UILabel com o texto padrão da célula
            .2 - UIActivityIndicatorView para mostrar que a requisição está sendo processada
    */
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(indexPath.row < laptops!.count ? "celulaLaptop" : "celulaCarregarMaisLaptops", forIndexPath: indexPath)
        // Ajustes da célula de acordo com o número de colunas
        if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
            cell.layer.cornerRadius = 5.0
        }
        else {
            cell.layer.cornerRadius = 0.0
        }
        // Configuração da célula do laptop
        if indexPath.row < laptops!.count {
            let imageViewLaptop = cell.viewWithTag(1) as! UIImageView
            if laptops![indexPath.item].urlImagem != nil {
                imageViewLaptop.sd_setImageWithURL(NSURL(string: laptops![indexPath.item].urlImagem!), placeholderImage: nil, completed: { (image, error, cachetype, url) -> Void in
                    // Exibição de uma imagem genérica para o caso de falha de carregamento da imagem original
                    if error != nil {
                        imageViewLaptop.image = UIImage(named: "imgLaptop")
                    }
                })
            }
            let labelNomeLaptop = cell.viewWithTag(2) as! UILabel
            labelNomeLaptop.text = laptops![indexPath.item].nome
            let labelFabricante = cell.viewWithTag(3) as! UILabel
            labelFabricante.text = laptops![indexPath.item].fabricante
            let labelPreco = cell.viewWithTag(4) as! UILabel
            labelPreco.text = "US$\(laptops![indexPath.item].preco)"
        }
        // Configuração da célula de mais conteúdo
        else {
            celulaCarregarMaisLaptops = cell
            celulaCarregarMaisLaptops.viewWithTag(1)?.hidden = carregandoMaisLaptops
            celulaCarregarMaisLaptops.viewWithTag(2)?.hidden = !carregandoMaisLaptops
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        // Para a Size Class Regular de largura é exibido um grid com 2 colunas
        return CGSizeMake(self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular ?collectionView.frame.size.width / 2 - 5 : collectionView.frame.size.width, 150)
    }
    

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        // Quando os laptops são exibidos em 2 colunas, é utilizado um espaçamento 10. Em uma coluna, é usado o espaçamento 1.
        return self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular ? 10 : 1
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        // Quando uma célula de produto é selecionada, é exibida a tela de detalhes do produto
        if indexPath.row < laptops!.count {
            laptopSelecionado = indexPath.item
            self.performSegueWithIdentifier("segueDetalhesLaptop", sender: self)
        }
        // Quando a última célula é seleciona, é feita a requisição para carregar a próxima página de produtos
        else {
            if !carregandoMaisLaptops {
                carregarProximaPaginaLaptops()
            }
        }
    }
    
    func orientacaoTrocada(notification : NSNotification) {
        //Recarregamento da tabela após uma rotação para reajustar o tamanho das células
        collectionViewLaptops.reloadData()
    }
    
    // Método para carregar conteúdo assim que a tela é exibida pela primeira vez.
    private func carregarLaptopsPelaPrimeiraVez() {
        AGFGerenciadorLaptop.laptopsEmDestaque(callback: { listaLaptops in
            self.indicadorAtividade.hidden = true
            self.laptops = listaLaptops
            // Sucesso
            if self.laptops?.count > 0 {
                self.collectionViewLaptops.hidden = false
                self.collectionViewLaptops.reloadData()
            }
            // Sucesso, porém não foram retornados produtos
            else if self.laptops?.count == 0 {
                self.labelInformacao.hidden = false
                self.labelInformacao.text = NSLocalizedString("semLaptops", comment: "")
                self.botaoTentarNovamente.hidden = false
            }
            // Erro de rede
            else {
                self.labelInformacao.hidden = false
                self.labelInformacao.text = NSLocalizedString("erroRede", comment: "")
                self.botaoTentarNovamente.hidden = false
            }
        })
    }
    
    // Método para carregar os produtos da próxima página
    private func carregarProximaPaginaLaptops() {
        // Mudança no flag para evitar concorrência
        carregandoMaisLaptops = true
        // Indicação do processamento na interface da célula tocada
        celulaCarregarMaisLaptops.viewWithTag(1)?.hidden = true
        celulaCarregarMaisLaptops.viewWithTag(2)?.hidden = false
        AGFGerenciadorLaptop.laptopsEmDestaque(paginaAtual + 1, callback: { listaLaptops in
            self.celulaCarregarMaisLaptops.viewWithTag(1)?.hidden = true
            self.celulaCarregarMaisLaptops.viewWithTag(2)?.hidden = false
            // Sucesso
            if listaLaptops != nil {
                if listaLaptops!.count > 0 {
                    self.laptops!.appendContentsOf(listaLaptops!)
                    self.collectionViewLaptops.reloadSections(NSIndexSet(index: 0))
                    self.paginaAtual++
                }
            }
            // Erro de Rede
            else {
                let alertController = UIAlertController(title: NSLocalizedString("erroRede", comment: ""), message: NSLocalizedString("naoFoiPossivelCarregarMaisLaptops", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
                alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { action in }))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            self.carregandoMaisLaptops = false
        })
    }
    
    // Método que refaz a primeira requisição de conteúdos após uma falha
    @IBAction func tentarCarregarLaptopsNovamente(sender: AnyObject) {
        self.botaoTentarNovamente.hidden = true
        self.labelInformacao.hidden = true
        self.indicadorAtividade.hidden = false
        carregarLaptopsPelaPrimeiraVez()
    }
    
    // Método vinculado ao UIRefreshControl da Collection View para que o usuário recarregue manualmente os produtos exibidos
    func recarregarLaptops() {
        AGFGerenciadorLaptop.laptopsEmDestaque(callback: { listaLaptops in
            self.laptops = listaLaptops
            if self.laptops?.count == 0 {
                self.collectionViewLaptops.hidden = true
                self.labelInformacao.hidden = false
                self.labelInformacao.text = NSLocalizedString("semLaptops", comment: "")
                self.botaoTentarNovamente.hidden = false
                self.paginaAtual = 1
            }
            self.collectionViewLaptops.reloadData()
            self.refreshControl.endRefreshing()
        })
    }
    
    // Quando o usuário muda o critério de ordenação preferido os conteúdos exibidos são removidos e recarregados
    func tratarAlteracaoPreferenciaOrdenacao() {
        if laptops != nil {
            laptops!.removeAll()
            indicadorAtividade.hidden = false
            labelInformacao.hidden = true
            collectionViewLaptops.hidden = true
            carregarLaptopsPelaPrimeiraVez()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueDetalhesLaptop" {
            // Atribuição da variável laptop da tela de detalhes antes de sua exibição
            (segue.destinationViewController as! AGFDetalhesLaptopViewController).laptop = laptops![laptopSelecionado]
            laptopSelecionado = -1
        }
    }
    
}


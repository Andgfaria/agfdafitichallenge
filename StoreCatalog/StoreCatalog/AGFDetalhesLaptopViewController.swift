/*  AGFDetalhesLaptopViewController - classe criada por André Gimenez Faria em janeiro de 2016
    UIViewController para a exibição dos detalhes dos laptops
    Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit
import SDWebImage
import SafariServices

class AGFDetalhesLaptopViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var laptop : AGFLaptop!

    var imagemGrandeLaptop : UIImage?
    
    @IBOutlet var tableViewDetalhesLaptop: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Notificação para tratar a mudança de orientação dos dispositivos
        UIDevice.currentDevice().beginGeneratingDeviceOrientationNotifications()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("orientacaoTrocada:"), name: UIDeviceOrientationDidChangeNotification, object: nil)
        // Com uma footer view vazia não são exibidos os separadores abaixo das células utilizadas
        tableViewDetalhesLaptop.tableFooterView = UIView(frame: CGRectZero)
        // Recarregamento da tabela para garantir que a altura das células seja calculada devidamente com o AutoLayout
        self.tableViewDetalhesLaptop.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Esvaziamento do cache do SDImage
        SDImageCache.sharedImageCache().clearMemory()
        SDImageCache.sharedImageCache().cleanDisk()
    }
    
    /* A interface dessa classe varia de acordo com a largura da tela
       Em telas com largura maior são exibidas 2 células e em telas com largura menor 5 células
    */
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular ? 2 : 5
    }
    
    func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPath.row == 0 ? 150 : 56
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        // Altura da célula da imagem grande
        if indexPath.row == 0 {
            // Altura maior para iPads
            if self.view.traitCollection.horizontalSizeClass == .Regular && self.view.traitCollection.verticalSizeClass == .Regular {
                return 0.6 * self.view.frame.size.height
            }
            // Altura nos iPhones
            return UIDeviceOrientationIsLandscape(UIDevice.currentDevice().orientation) ? 0.4 * self.view.frame.size.height  :  0.3 * self.view.frame.size.height
        }
        // Altura da célula com o preço do laptop
        else if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact && indexPath.row == 3  {
            return 64
        }
        // Células com altura dinâmica
        else {
            return UITableViewAutomaticDimension
        }
    }
    
    // Seleção da célula apropriada pelo indexPath da tabela
    private func reuseIdentifierParaIndexPath(indexPath : NSIndexPath) -> String {
        if indexPath.row == 0 {
            return "celulaImagemGrande"
        }
        else if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular && indexPath.row == 1 {
            return "celulaResumoProduto"
        }
        else if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Compact && indexPath.row == 3 {
            return "celulaPreco"
        }
        else {
            return "celulaTextoSimples"
        }
    }
    
    /* 4 tipos de células são utilizados aqui
       CelulaImagemGrande - exibe as informações básicas dos laptops
            .1 - UIImageView para exibir a foto grande do laptop
       CelulaResumoProduto - todas as informações do produto em uma célula só com layout especial para maior largura
            .1 - UILabel para exibir o nome do laptop
            .2 - UILabel para exibir o nome da fabricante do laptop
            .3 - UIButton para poder adicionar o produto no carrinho, se disponível
            .4 - UITextView com a descrição do produto
       CelulaTextoSimples - célula default do iOS
       CelulaPreco - célula com a exibição do preço e disponibilidade do produto
    */
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = reuseIdentifierParaIndexPath(indexPath)
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier, forIndexPath: indexPath)
        let stringPreco = "US$\(laptop.preco)"
        // AttributedString para exibir o preço com fonte maior e a disponibilidade com fonte menor
        let attributedStringPreco = NSMutableAttributedString(string: stringPreco + (laptop.disponivelOnline! ? "\n"
        + NSLocalizedString("comprar", comment: "") : "\n" + NSLocalizedString("disponivelLojas", comment: "")))
        attributedStringPreco.addAttribute(NSFontAttributeName, value: UIFont.boldSystemFontOfSize(21), range: NSMakeRange(0, stringPreco.characters.count))
        attributedStringPreco.addAttribute(NSFontAttributeName, value: UIFont.systemFontOfSize(13), range: NSMakeRange(stringPreco.characters.count, attributedStringPreco.length - stringPreco.characters.count))
        // Célula para a imagem grande
        if indexPath.row == 0 {
            let imageViewLaptop = cell.viewWithTag(1) as! UIImageView
            if laptop.urlImagemGrande != nil {
                imageViewLaptop.sd_setImageWithURL(NSURL(string: laptop.urlImagemGrande!), placeholderImage: nil, completed: { (image, error, cachetype, url) -> Void in
                    if error != nil {
                        imageViewLaptop.image = UIImage(named: "imgLaptop")
                    }
                    else {
                        // Armazenamento da imagem grande do produto para que possa ser compartilhada
                        self.imagemGrandeLaptop = image
                    }
                })
            }
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
        }
        // Célula com o resumo do produto para largura grande
        else if self.view.traitCollection.horizontalSizeClass == UIUserInterfaceSizeClass.Regular {
            let labelNomeLaptop = cell.viewWithTag(1) as! UILabel
            labelNomeLaptop.text = laptop.nome
            let labelFabricante = cell.viewWithTag(2) as! UILabel
            labelFabricante.text = laptop.fabricante
            let botaoComprar = cell.viewWithTag(3) as! UIButton
            botaoComprar.layer.cornerRadius = 10.0
            botaoComprar.layer.masksToBounds = false
            botaoComprar.layer.borderWidth = 2.0
            botaoComprar.layer.borderColor = UIColor(red: 241/255, green: 201/255, blue: 45/255, alpha: 1).CGColor
            botaoComprar.setAttributedTitle(attributedStringPreco, forState: UIControlState.Normal)
            botaoComprar.titleLabel?.textColor = UIColor.darkTextColor()
            botaoComprar.titleLabel?.numberOfLines = 0
            botaoComprar.titleLabel?.lineBreakMode = NSLineBreakMode.ByWordWrapping
            botaoComprar.titleLabel?.textAlignment = NSTextAlignment.Center
            botaoComprar.sizeToFit()
            botaoComprar.addTarget(self, action: Selector("adicionarNoCarrinho"), forControlEvents: UIControlEvents.TouchUpInside)
            let textViewDescricaoLaptop = cell.viewWithTag(4) as! UITextView
            textViewDescricaoLaptop.text = laptop.descricao
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0)
        }
        else {
            // Célula com o nome do laptop
            if indexPath.row == 1 {
                cell.textLabel?.text = laptop.nome
                cell.textLabel?.font = UIFont.systemFontOfSize(18)
                cell.textLabel?.sizeToFit()
            }
            // Célula com o nome da fabricante
            else if indexPath.row == 2 {
                cell.textLabel?.text = laptop.fabricante
                cell.textLabel?.font = UIFont.systemFontOfSize(14)
                cell.textLabel?.textColor = UIColor.lightGrayColor()
            }
            // Célula com o preço
            else if indexPath.row == 3 {
                let labelPreco = cell.viewWithTag(1) as! UILabel
                labelPreco.attributedText = attributedStringPreco
            }
            // Célula com a descrição do laptop
            else if indexPath.row == 4 {
                cell.textLabel?.text = laptop.descricao
                cell.textLabel?.font = UIFont.systemFontOfSize(14)
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let identifierCelula = reuseIdentifierParaIndexPath(indexPath)
        // Seleção da imagem grande
        if identifierCelula == "celulaImagemGrande" {
            if imagemGrandeLaptop != nil {
                // Exibição da imagem em uma tela separada com a opção de zoom
                self.performSegueWithIdentifier("segueFocoImagem", sender: self)
            }
        }
        // Seleção da célula de preço
        else if identifierCelula == "celulaPreco" {
            adicionarNoCarrinho()
        }
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // Método para compartilhar a imagem, o nome e a url do laptop
    @IBAction func compartilhar(sender: AnyObject) {
        var itensParaCompartilhar : [AnyObject] = [laptop.nome, laptop.url]
        if imagemGrandeLaptop != nil {
            itensParaCompartilhar.insert(imagemGrandeLaptop!, atIndex: 0)
        }
        let activityViewController = UIActivityViewController(activityItems: itensParaCompartilhar, applicationActivities: nil)
        activityViewController.popoverPresentationController?.barButtonItem = sender as? UIBarButtonItem
        self.presentViewController(activityViewController, animated: true, completion: nil)
    }
    
    // Método para exibir a página de compra do produto em uma SafariViewController
    func adicionarNoCarrinho() {
        let safariViewController = SFSafariViewController(URL: NSURL(string: laptop.urlCarrinho)!)
        self.presentViewController(safariViewController, animated: true, completion: nil)
    }
    
    func orientacaoTrocada(notification : NSNotification) {
        //Recarregamento da tabela após uma rotação para reajustar o tamanho das células
        tableViewDetalhesLaptop.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "segueFocoImagem" {
            let viewControllerFocoImagem = (segue.destinationViewController as! UINavigationController).childViewControllers.last! as! AGFFocoImagemViewController
            viewControllerFocoImagem.imagem = imagemGrandeLaptop!
        }
    }
    
}

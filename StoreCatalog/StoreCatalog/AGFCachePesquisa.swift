/* AGFCachePesquisa - classe criada por André Gimenez Faria em janeiro de 2016
   Classe para gerenciar a entrada de texto de pesquisa do usuário
   Este código está sob a licença MIT, disponível em (https://opensource.org/licenses/MIT)
*/

import UIKit

class AGFCachePesquisa: NSObject {
    
    // Texto inserido pelo usuário
    var textoPesquisa : String = "" {
        didSet {
           tokensPesquisa = !textoPesquisa.isEmpty ? textoPesquisa.componentsSeparatedByString(" ") : []
        }
    }
    
    // Vetor contendo o texto do usuário separado em tokens
    var tokensPesquisa : [String] = [] {
        didSet {
            if tokensPesquisa.count > 0 {
                AGFGerenciadorLaptop.sharedInstance.pesquisar(tokensPesquisa, callback: { listaLaptops in
                    self.laptops = listaLaptops
                    NSNotificationCenter.defaultCenter().postNotificationName("pesquisaConcluida", object: self, userInfo: ["textoPesquisa" : self.textoPesquisa])
                })
            }
            else {
                AGFGerenciadorLaptop.sharedInstance.cancelarPesquisa()
            }
        }
    }
    
    // Vetor com os laptops provenientes da pesquisa
    var laptops : [AGFLaptop]?
    
    // Singleton da classe
    class var sharedInstance : AGFCachePesquisa {
        struct Static {
            static var instance : AGFCachePesquisa?
            static var token    : dispatch_once_t = 0
        }
        dispatch_once(&Static.token) {
            Static.instance = AGFCachePesquisa()
        }
        return Static.instance!
    }
    
    func limpar() {
        textoPesquisa = ""
        tokensPesquisa.removeAll()
        laptops?.removeAll()
    }

}
